<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnimalsTable extends Migration
{
    public function up()
    {
        Schema::create('animals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('weight');
            $table->integer('gender');
            $table->integer('age');
            $table->boolean('species');
            $table->integer('class_id');
            $table->timestamps();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('animals');
    }
}
