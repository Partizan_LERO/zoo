<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'AnimalsController@index');

Route::post('add-animal', 'AnimalsController@createAnimal');
Route::delete('/{id}', 'AnimalsController@deleteAnimal');

Route::post('add-class', 'ClassesController@createClass');