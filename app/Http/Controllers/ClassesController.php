<?php

namespace App\Http\Controllers;

use App\Models\BioClass;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ClassesController extends Controller
{
    public function createClass(Request $request) {
        $class = new BioClass();
        $class->name = $request->name;
        $class->save();
        return redirect('/');
    }
}
