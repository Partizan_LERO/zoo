<?php

namespace App\Http\Controllers;

use App\Models\Animal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class AnimalsController extends Controller
{
    public function createAnimal(Request $request) {
        $animal = new Animal();
        $animal->name = $request->name;
        $animal->weight = $request->weight;
        $animal->age = $request->age;
        $animal->gender = $request->gender;
        $animal->class_id = $request->class;
        $animal->species = $request->species;
        $animal->save();
        return redirect('/');
    }

    public function deleteAnimal($id) {
        $animal = Animal::find($id);
        $animal->delete();
        return redirect('/');
    }

    public function index() {
        $animals = DB::table('animals')
            ->select(
                'animals.*',
                'classes.name as bioclass'
            )
            ->join('classes', 'animals.class_id', '=', 'classes.id')
            ->get();
        $classes = DB::table('classes')->get();
        return view('index', compact('animals', 'classes'));
    }
}
