<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BioClass extends Model
{
    protected $table = 'classes';
}
