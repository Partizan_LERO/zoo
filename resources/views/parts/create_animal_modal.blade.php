<div id="create-animal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add animal</h4>
            </div>
            <div class="modal-body">
                <form action="add-animal" method="POST">
                    {{csrf_field()}}
                    <div class="form-group row">
                        <div class="col-sm-6">
                            <label for="name">Name:</label>
                            <input type="text" class="form-control" name="name" id="name">
                        </div>
                        <div class="col-sm-3">
                            <label for="weight">Weight:</label>
                            <input type="number" class="form-control" name="weight" id="weight">
                        </div>
                        <div class="col-sm-3">
                            <label for="age">Age:</label>
                            <input type="number" class="form-control" name="age" id="age">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <label for="age">Gender:</label>
                            <input type="radio" name="gender" value="0"> Male
                            <input type="radio" name="gender" value="1"> Female
                            <input type="radio" name="gender" value="2"> Unknown
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6">
                            <label for="class">Class:</label>
                            <select name="class" id="class">
                                @foreach($classes as $class)
                                    <option value="{{$class->id}}">{{$class->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <label for="species">Species:</label>
                            <input type="radio" name="species" value="1"> Predator
                            <input type="radio" name="species" value="0"> Herbivore
                        </div>
                    </div>

                    <button type="submit" class="btn btn-success">Add</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>