@extends('layouts.index')
@section('content')
    <div class="form-group">
        <button class="btn btn-primary" data-toggle="modal" data-target="#create-animal">Add animal</button>

        <button class="btn btn-warning" data-toggle="modal" data-target="#create-class">Add class</button>
    </div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Name</th>
                <th>Gender</th>
                <th>Age</th>
                <th>Weight</th>
                <th>Class</th>
                <th>Species</th>
            </tr>
        </thead>
        <tbody>
@foreach($animals as $animal)
            <tr>
                <td>{{$animal->name}}</td>
                <td>@if($animal->gender == 0) Male @elseif($animal->gender == 1) Female @else Unknown @endif</td>
                <td>{{$animal->age}}</td>
                <td>{{$animal->weight}} kg</td>
                <td>{{$animal->bioclass}}</td>
                <td>@if($animal->species == 1) Predator @else Herbivore @endif</td>
                <td>
                    <form action="/{{$animal->id}}" method="POST">
                        {{ method_field('DELETE') }}
                        {{csrf_field()}}
                        <button class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-remove"></i></button>
                    </form>
                </td>
            </tr>
@endforeach
        </tbody>
    </table>
    @include('parts.create_animal_modal')
    @include('parts.create_class_modal')
@endsection